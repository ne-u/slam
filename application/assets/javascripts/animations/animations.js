import gsap from 'gsap';
import { qs, qsa, viewportSize, isMobile, addClass, hasClass } from '../base/utils';

const SplitText = require('../../lib/gsap/uncompressed/utils/SplitText');

const animations = new Map();

const splitArray = [];

animations.set('header', (ctx, type, hover) => {

    switch (type) {
    case 'set':
        gsap.set(ctx, { y: -50, autoAlpha: 0 });
        gsap.set(qsa('.dropdowns [data-menu]', ctx), { scaleY: 0, transformOrigin: '0 0' });
        gsap.set(qsa('.dropdowns [data-menu] .nav', ctx), { autoAlpha: 0 });
        gsap.set(qsa('.menu [data-hover] .menu__hover__bg', ctx), { scaleY: 0, autoAlpha: 0, transformOrigin: '0 100' });
        gsap.set(qsa('.logo__bg', ctx), { autoAlpha: 0 });
        break;
    case 'init':
        gsap.to(ctx, 0.8, {
            autoAlpha: 1,
            y: 0,
            ease: gsap.Expo.easeOut,
            delay: 1
        });
        break;
    case 'hide':
        gsap.to(ctx, 0.8, {
            y: -100,
            autoAlpha: 0,
            ease: gsap.Expo.easeOut
        });
        break;
    case 'fixed':
        gsap.to(ctx, 0.8, {
            autoAlpha: 1,
            y: 0,
            backgroundColor: '#ffffff',
            ease: gsap.Expo.easeOut
        });
        break;
    case 'top':
        if (hasClass(document.body, 'page-home') || hasClass(document.body, 'page-discover') || hasClass(document.body, 'page-stories')) {
            gsap.to(ctx, 0.8, {
                backgroundColor: 'transparent',
                ease: gsap.Expo.easeOut
            });
        }
        if (hasClass(document.body, 'page-payment') || hasClass(document.body, 'page-register')) {
            gsap.set(ctx, { clearProps: 'all' });
        }
        break;
    case 'hover':
        gsap.set(qsa('.dropdowns [data-menu]', ctx), { scaleY: 0, transformOrigin: '0 0' });
        gsap.set(qs(`.dropdowns [data-menu="${hover}"]`, ctx), { position: 'relative' });
        gsap.set(qs(`.menu [data-hover="${hover}"]`, ctx), { color: '#ffffff' });
        if (hover === 'hamburger') gsap.set(qs('.menu [data-hover="hamburger"] svg path', ctx), { fill: '#ffffff' });

        gsap.to(qs(`.menu [data-hover="${hover}"] .menu__hover__bg`, ctx), 0.2, {
            autoAlpha: 1,
            scaleY: 1,
            transformOrigin: '0 100%',
            ease: gsap.Expo.easeOut
        });

        gsap.to(ctx, 0.8, {
            backgroundColor: '#ffffff',
            ease: gsap.Expo.easeOut
        });
        
        gsap.to(qs(`.dropdowns [data-menu="${hover}"] .logo__bg`, ctx), 0.5, {
            autoAlpha: 0.05,
            delay: 0.2
        });

        gsap.to(qs(`.dropdowns [data-menu="${hover}"]`, ctx), 0.5, {
            scaleY: 1,
            transformOrigin: '0 0',
            ease: gsap.Expo.easeOut
        });

        gsap.staggerTo(qsa(`.dropdowns [data-menu="${hover}"] .nav`, ctx), 0.4, {
            autoAlpha: 1,
            ease: gsap.Expo.easeOut,
            delay: 0.4
        }, 0.02);

        break;
    case 'hover-leave':

        gsap.set(qs('.menu [data-hover="hamburger"] svg path', ctx), { clearProps: 'all' });
        gsap.set(qsa('.logo__bg', ctx), { autoAlpha: 0 });
        gsap.set(qsa('.dropdowns [data-menu]', ctx), { clearProps: 'position' });
        if (hover !== undefined) {
            gsap.set(qs(`.menu [data-hover="${hover}"]`, ctx), { clearProps: 'all' });

            gsap.to(qs(`.menu [data-hover="${hover}"] .menu__hover__bg`, ctx), 0.2, {
                scaleY: 0,
                autoAlpha: 0,
                transformOrigin: '0 100%',
                ease: gsap.Expo.easeOut,
                delay: 0.1
            });

            gsap.to(qsa(`.dropdowns [data-menu="${hover}"] .nav`, ctx), 0.1, {
                autoAlpha: 0,
                ease: gsap.Expo.easeOut
            });

            gsap.to(qs(`.dropdowns [data-menu="${hover}"]`, ctx), 0.4, {
                scaleY: 0,
                transformOrigin: '0 0',
                delay: 0.1,
                ease: gsap.Expo.easeOut
            });
        } else {
            gsap.set(qsa('.menu [data-hover]', ctx), { clearProps: 'all' });

            gsap.to(qsa('.menu [data-hover] .menu__hover__bg', ctx), 0.2, {
                scaleY: 0,
                autoAlpha: 0,
                transformOrigin: '0 0',
                ease: gsap.Expo.easeOut,
                delay: 0.1
            });

            gsap.to(qsa('.dropdowns [data-menu] .nav', ctx), 0.1, {
                autoAlpha: 0,
                ease: gsap.Expo.easeOut
            });

            gsap.to(qsa('.dropdowns [data-menu]', ctx), 0.4, {
                scaleY: 0,
                transformOrigin: '0 0',
                delay: 0.1,
                ease: gsap.Expo.easeOut
            });
        }
        break;
    default:
        break;
    }

});

animations.set('home', (ctx, init) => {

    const splitLastStories = new SplitText('.l-story__title', { type: 'lines', linesClass: 'lines' });

    const tl = new gsap.TimelineMax({
        paused: true
    });

    if (init) {
        tl.add('start')
        .set('.hero', { y: viewportSize().y / 3 })
        .set('.slider__navigator__line', { scaleX: 0, transformOrigin: '0 0' })
        .set('.slider__navigator', { scaleX: 0, transformOrigin: '100% 0' })
        .set('.slider__navigator > div', { autoAlpha: 0, y: -5 })
        .set('.hero__item', { autoAlpha: 0 });
        
    } else {
        tl.add('start')
        .set('.slider__navigator__line', { scaleX: 0, transformOrigin: '0 0' })
        .to('.hero', 0.8, {
            y: 0,
            ease: gsap.Expo.easeOut
        })
        .to('.slider__navigator', 0.6, {
            scaleX: 1,
            ease: gsap.Expo.easeOut
        }, 'start+=1.2')
        .staggerTo('.slider__navigator > div', 0.2, {
            autoAlpha: 1,
            y: 0,
            ease: gsap.Power2.easeOut
        }, 0.02, 'start+=1.6');
    }

    tl.play();
});

animations.set('hero', (type, ctx, dir, init, ex, index, indexEx) => {

    const splitTitle = new SplitText(qs('.hero__title h2', ctx), { type: 'lines, chars', charsClass: 'char', linesClass: 'line' });
    splitArray[index] = splitTitle._originals[0];

    const tl = new gsap.TimelineMax({
        paused: true,
        onStart: () => {
            if (ex) {
                //console.log(splitArray[index - 1]);
                qs('.hero__title h2', ex).innerHTML = splitArray[indexEx];
            }

        }
    });

    tl.add('start')
    .set(ctx, { autoAlpha: 1 })
    .set(qs('.hero__title__line', ctx), { scaleX: 0, transformOrigin: '100% 0' })
    .set(splitTitle.chars, { yPercent: -25, autoAlpha: 0 })
    .set(splitTitle.lines, { rotationZ: -5, rotationY: -5, transformOrigin: '0 100% 0' })
    .set(qs('.hero__title', ctx), { autoAlpha: 1 })
    .set(qs('.hero__bg', ctx), { scale: 1.2, transformOrigin: '50% 0%' })
    .set(ctx, { transformOrigin: '50% 0%', yPercent: 100 });
    if (ex) tl.set(ex, { transformOrigin: '50% 100%', yPercent: 0 });
    if (ex) {
        tl.to(qs('.hero__title', ex), 0.2, {
            autoAlpha: 0,
            ease: gsap.Expo.easeOut
        })
        .to(ex, 0.8, {
            yPercent: -100,
            ease: gsap.Expo.easeOut
        }, 'start')
    }
    tl.to(ctx, 0.8, {
        yPercent: 0,
        ease: gsap.Expo.easeOut
    }, 'start')
    .to(qs('.hero__bg', ctx), 2, {
        scale: 1,
        ease: gsap.Expo.easeOut
    }, 'start')
    .staggerTo(splitTitle.lines, 1.2, {
        rotationZ: 0,
        rotationY: 0,
        ease: gsap.Expo.easeOut
    }, 0.015, 'start+=0.75')
    .staggerTo(splitTitle.chars, 1, {
        yPercent: 0,
        autoAlpha: 1,
        ease: gsap.Expo.easeOut
    }, 0.015, 'start+=0.75')
    .to(qs('.hero__title__line', ctx), 0.4, {
        scaleX: 1,
        ease: gsap.Expo.easeOut
    }, 'start+=1.5');

    tl.play();

});

animations.set('counter', (last, current, init) => {
    const tl = new gsap.TimelineMax({
        paused: true
    });

    tl.add('start')
    .set('.slider__navigator__line', { scaleX: 0, transformOrigin: '0 0' })
    .set(last, { scaleX: 1 })
    .to(last, 0.2, {
        scaleX: 0,
        transformOrigin: '100% 0',
        ease: gsap.Power2.easeOut
    })
    .to(current, 0.2, {
        scaleX: 1,
        ease: gsap.Power2.easeOut
    }, 'start');

    if (init) {
        tl.play().delay(2.5);
    } else {
        tl.play();
    }
});

animations.set('hero-info', (el, state) => {
    gsap.set(qs('.hero__info__dots__bg', el), { scale: 1.5 });

    const dotLoop = gsap.to(qs('.hero__info__dots__bg', el), 0.8, {
        scale: 2,
        repeat: -1,
        yoyo: true,
        ease: gsap.Power2.easeInOut
    });

    if (state === 'hover') {
        dotLoop.pause();
        gsap.to(qs('.hero__info__dots__bg', el), 0.8, {
            scale: 1,
            ease: gsap.Power2.easeInOut
        });
        gsap.to(qs('span', el), 0.4, {
            autoAlpha: 1,
            marginLeft: 0,
            ease: gsap.Power2.easeOut
        });
    } else if (state === 'leave') {
        gsap.to(qs('span', el), 0.4, {
            autoAlpha: 0,
            ease: gsap.Power2.easeOut
        });
    } else {
        gsap.set(qs('span', el), { autoAlpha: 0, marginLeft: -10 });
    }

});

animations.set('tab-fx', (ex, el) => {
    const tl = new gsap.TimelineMax();

    tl.add('start')
    .set(ex, { autoAlpha: 1, yPercent: 0 })
    .set(el, { yPercent: 0 })
    .set(qs('img', el), { scale: 1 })
    .to(ex, 1.2, {
        yPercent: -100,
        ease: gsap.Expo.easeOut
    }, 0)
    .from(el, 1.2, {
        yPercent: 100,
        ease: gsap.Expo.easeOut
    }, 0)
    .from(qs('img', el), 1.2, {
        scale: 1.5,
        transformOrigin: '50% 0%',
        ease: gsap.Expo.easeOut
    }, 0);

});

animations.set('tab-fade', (ex, el) => {
    const tl = new gsap.TimelineMax();

    tl.add('start')
    .set(ex, { autoAlpha: 1 })
    .set(el, { autoAlpha: 1 })
    .set(qsa('img', el), { autoAlpha: 0 })
    .to(ex, 1.2, {
        autoAlpha: 0,
        ease: gsap.Expo.easeOut
    }, 0)
    .staggerTo(qsa('img', el), 0.8, {
        autoAlpha: 1,
        ease: gsap.Expo.easeOut
    }, 0.05, 0.4);

});

animations.set('tab-zoom', (ex, el) => {
    const tl = new gsap.TimelineMax();

    tl.add('start')
    .set(ex, { autoAlpha: 0 })
    .set(el, { autoAlpha: 1 })
    .fromTo(qs('img', el), 1.4, {
        scale: 1.05, transformOrigin: '50% 100%'
    }, {
        scale: 1,
        ease: gsap.Expo.easeOut
    });

});

animations.set('inview', (el) => {
    gsap.set(el, { autoAlpha: 0 });
    gsap.set(qs('img', el), { scale: 1, yPercent: 0 });
    gsap.to(el, 1.2, {
        autoAlpha: 1,
        ease: gsap.Expo.easeOut
    });
    gsap.from(qs('img', el), 1.2, {
        yPercent: 100,
        scale: 1.5,
        transformOrigin: '50% 0%',
        clearProps: 'all',
        ease: gsap.Expo.easeOut,
    });
});

animations.set('story-title', (el) => {

    const splitTitle = new SplitText(el, { type: 'lines, chars', charsClass: 'char', linesClass: 'line' });

    const tl = new gsap.TimelineMax();
   
    tl.set(el, { autoAlpha: 1 })
    .set(splitTitle.chars, { yPercent: -25, autoAlpha: 0 })
    .set(splitTitle.lines, { rotationZ: -5, rotationY: -5, transformOrigin: '0 100% 0' })
    .staggerTo(splitTitle.lines, 1.2, {
        rotationZ: 0,
        rotationY: 0,
        ease: gsap.Expo.easeOut
    }, 0.015)
    .staggerTo(splitTitle.chars, 1, {
        yPercent: 0,
        autoAlpha: 1,
        ease: gsap.Expo.easeOut
    }, 0.015, 0.5);
});

animations.set('inview-title', (el) => {

    const splitTitle = new SplitText(el, { type: 'lines, chars', charsClass: 'char', linesClass: 'line' });

    const tl = new gsap.TimelineMax();

    tl.set(el, { autoAlpha: 1 })
    .set(splitTitle.chars, { yPercent: -25, autoAlpha: 0 })
    .set(splitTitle.lines, { rotationZ: -5, rotationY: -5, transformOrigin: '0 100% 0' })
    .staggerTo(splitTitle.lines, 1.2, {
        rotationZ: 0,
        rotationY: 0,
        ease: gsap.Expo.easeOut
    }, 0.015)
    .staggerTo(splitTitle.chars, 1, {
        yPercent: 0,
        autoAlpha: 1,
        ease: gsap.Expo.easeOut
    }, 0.015, 0.5);
});

animations.set('inview-subtitle', (el) => {

    const splitSubtitle = new SplitText(el, { type: 'words', wordsClass: 'word' });

    const tl = new gsap.TimelineMax();

    tl.set(el, { autoAlpha: 1 })
    .set(splitSubtitle.words, { autoAlpha: 0, yPercent: -100 })
    .staggerTo(splitSubtitle.words, 0.7, {
        yPercent: 0,
        autoAlpha: 1,
        ease: gsap.Expo.easeOut
    }, 0.03, 0.5);
});

animations.set('inview-article', (el) => {

    const paragraphs = qsa('.col-p p', el);
    gsap.staggerFromTo(paragraphs, 1.6, {
        opacity: 0,
        y: 30
    }, {
        y: 0,
        opacity: 1,
        ease: gsap.Expo.easeOut
    }, 0.25);
});

animations.set('inview-share', (el) => {

    gsap.set(el, { scaleX: 0, transformOrigin: '0 50%'});
    gsap.set(el.children, { autoAlpha: 0 });
    gsap.to(el, 1, {
        scaleX: 1,
        ease: gsap.Expo.easeOut
    }, 0.5);
    gsap.to(el.children, 0.5, {
        autoAlpha: 1,
        ease: gsap.Expo.easeOut,
        delay: 0.75
    });
});

animations.set('inview-fade', (el) => {

    gsap.set(el, { autoAlpha: 0, y: 50 });
    gsap.to(el, 0.5, {
        delay: 0.5,
        y: 0,
        autoAlpha: 1,
        ease: gsap.Expo.easeOut,
        clearProps: 'all'
    });
});


animations.set('inview-stagger', (el) => {

    gsap.set(el.children, { opacity: 0, y: 30 });
    gsap.staggerTo(el.children, 0.6, {
        //delay: 0.25,
        y: 0,
        opacity: 1,
        ease: gsap.Expo.easeInOut,
        clearProps: 'all'
    }, 0.25);
});


animations.set('filter', (el, dir) => {
    const tl = new gsap.TimelineMax({
        paused: true
    });

    tl.set(qsa('.l-col', el), { autoAlpha: 0 })
    .fromTo(el, 0.6, {
        scaleY: 0,
        'max-height': 0,
        transformOrigin: '0 0'
    }, {
        scaleY: 1,
        'max-height': 600,
        transformOrigin: '0 0',
        ease: gsap.Expo.easeOut
    })
    .to(qsa('.l-col', el), 0.2, {
        autoAlpha: 1,
    }, 0.4);

    if (dir === 'enter') {
        tl.play();
    } else {
        tl.progress(1, false).reverse();
    }
});

animations.set('gallery', (el, ex, init, dir) => {

    if (init) {
        gsap.set(el, { autoAlpha: 0 });
        gsap.set(qs('img', el), { scale: 1, yPercent: 0 });
        gsap.to(el, 1.2, {
            autoAlpha: 1,
            ease: gsap.Expo.easeOut
        });
        gsap.from(qs('img', el), 1.2, {
            yPercent: 100,
            scale: 1.5,
            transformOrigin: '50% 0%',
            ease: gsap.Expo.easeOut,
        });
    } else {
        let delta = 1;
        let originCustom = '50% 0%';
        if (dir === 'prev') {
            delta = -1;
            originCustom = '50% 100%';
        }

        gsap.set(ex, { yPercent: 0, autoAlpha: 1 });
        gsap.set(el, { yPercent: 100 * delta, autoAlpha: 1 });
        gsap.set(qs('img', el), { scale: 1 });
        gsap.to(ex, 1.2, {
            yPercent: -100 * delta,
            ease: gsap.Expo.easeOut
        });
        gsap.to(el, 1.2, {
            yPercent: 0,
            ease: gsap.Expo.easeOut
        });
        gsap.from(qs('img', el), 1.2, {
            scale: 1.5,
            transformOrigin: originCustom,
            ease: gsap.Expo.easeOut,
        });
    }

});

animations.set('step', (el, init) => {

    const out = qs('.form', el);
    const ini = qs('.completed', el);

    if (init) {
        gsap.to(out, 0.5, { className: '+=heightZero', ease: gsap.Expo.easeOut });
        gsap.set(ini, { className: '-=heightZero' });
    } else {
        gsap.set(ini, { className: '+=heightZero' });
        gsap.to(out, 0.5, { className: '-=heightZero', ease: gsap.Expo.easeOut });
    }
});

animations.set('step-wrapper', (el) => {

    gsap.set(el, { display: 'block', yPercent: 40, autoAlpha: 0 });
    gsap.to(el, 0.8, {
        yPercent: 0,
        autoAlpha: 1,
        clearProps: 'all',
        ease: gsap.Expo.easeOut,
    });

});

animations.set('stories', (ctx) => {
    const splitLastStories = new SplitText('.l-story__title', { type: 'lines', linesClass: 'lines' });
});


export default animations;