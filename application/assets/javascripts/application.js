/**
 * Main Application File
 *
 * Use for bootstrapping large application
 * or just fill it with JS on small projects
 *
 */

/**
 * This is a test script
 */

import classie from 'classie';
import Barba from 'barba.js';
import * as manifest from './manifest.json';
import { data, qsa, qs, removeClassByPrefix, removeClass, hasClass, inViewport } from './base/utils';
import Loader from './components/loader';
import Header from './components/header';
import Cart from './components/cart';
import animations from './animations/animations';
import {
    GenericTransition
} from './transitions';

import './sections/generic';
import Homepage from './sections/home';
import './sections/products';
import './sections/story';
import './sections/stories';
import './sections/discover';
import './sections/payment';


const links = qsa('a[href]', document);
links.forEach((el) => {
    el.addEventListener('click', (e) => {
        if (e.currentTarget.href === window.location.href) {
            e.preventDefault();
            e.stopPropagation();
        }
    });
});

const animateIn = () => {
    const elsFx = qsa('[data-animate]', document);
    const titleFx = qsa('[data-title]', document);
    const subtitleFx = qsa('[data-subtitle]', document);
    const articleFx = qsa('[data-article]', document);
    const fadeFx = qsa('[data-fade]', document);
    const staggerFx = qsa('[data-stagger]', document);

    titleFx.forEach((el) => {
        if (inViewport(el) && !el.getAttribute('data-title') === true) {
            animations.get('inview-title')(el);
            el.setAttribute('data-title', true);
        }
    });
    document.addEventListener('scroll', () => {
        titleFx.forEach((el) => {
            if (inViewport(el) && !el.getAttribute('data-title') === true) {
                animations.get('inview-title')(el);
                el.setAttribute('data-title', true);
            }
        });
    });

    subtitleFx.forEach((el) => {
        if (inViewport(el) && !el.getAttribute('data-subtitle') === true) {
            animations.get('inview-subtitle')(el);
            el.setAttribute('data-subtitle', true);
        }
    });
    document.addEventListener('scroll', () => {
        subtitleFx.forEach((el) => {
            if (inViewport(el) && !el.getAttribute('data-subtitle') === true) {
                animations.get('inview-subtitle')(el);
                el.setAttribute('data-subtitle', true);
            }
        });
    });

    articleFx.forEach((el) => {
        if (inViewport(el) && !el.getAttribute('data-article') === true) {
            animations.get('inview-article')(el);
            el.setAttribute('data-article', true);
        }
    });
    document.addEventListener('scroll', () => {
        articleFx.forEach((el) => {
            if (inViewport(el) && !el.getAttribute('data-article') === true) {
                animations.get('inview-article')(el);
                el.setAttribute('data-article', true);
            }
        });
    });

    elsFx.forEach((el) => {
        if (inViewport(el) && !el.getAttribute('data-animate') === true) {
            animations.get('inview')(el);
            el.setAttribute('data-animate', true);
        }
    });
    document.addEventListener('scroll', () => {
        elsFx.forEach((el) => {
            if (inViewport(el) && !el.getAttribute('data-animate') === true) {
                animations.get('inview')(el);
                el.setAttribute('data-animate', true);
            }
        });
    });

    fadeFx.forEach((el) => {
        if (inViewport(el) && !el.getAttribute('data-fade') === true) {
            animations.get('inview-fade')(el);
            el.setAttribute('data-fade', true);
        }
    });
    document.addEventListener('scroll', () => {
        fadeFx.forEach((el) => {
            if (inViewport(el) && !el.getAttribute('data-fade') === true) {
                animations.get('inview-fade')(el);
                el.setAttribute('data-fade', true);
            }
        });
    });

    staggerFx.forEach((el) => {
        if (inViewport(el) && !el.getAttribute('data-stagger') === true) {
            animations.get('inview-stagger')(el);
            el.setAttribute('data-stagger', true);
        }
    });
    document.addEventListener('scroll', () => {
        staggerFx.forEach((el) => {
            if (inViewport(el) && !el.getAttribute('data-stagger') === true) {
                animations.get('inview-stagger')(el);
                el.setAttribute('data-stagger', true);
            }
        });
    });
}


const header = new Header(qs('#header'));
const modal = new Cart(qs('[data-modal="cart"]'));

// const noise = new Noise();
// noise.loop();


document.addEventListener('DOMContentLoaded', () => {
    const menuVoices = qsa('[data-nav]');
    const getMenuVoice = (els, namespace) => els.find(el => el.getAttribute('data-namespace', namespace) === namespace);
    let lastElementClicked;
    let currentNamespace;
    
    // start up
    const loader = new Loader('.loader', { assets: manifest });
    header.init();
    Homepage.init();
    
    if (qs('.barba-container[data-namespace="home"]') !== null) animations.get('home')('[data-namespace="home"]', true);

    
    
    loader.init().then(() => {
        Barba.Pjax.start();
        Barba.Prefetch.init();
        loader.destroy();
        header.animateIn();
        
        animateIn();

        modal.init();

    });

    qs('.btn--to-payment').addEventListener('click', () => {
        event.preventDefault();
        modal.close();
        setTimeout(() => {
            Barba.Pjax.goTo('payment.html');
        }, 800);
    });

    qs('.cart a').addEventListener('click', () => {
        event.preventDefault();
        if (hasClass(document.body, 'page-home')) header.isFixed();
        modal.open();
    });

    // get current namespace
    Barba.Dispatcher.on('linkClicked', (el) => {
        lastElementClicked = el;
        currentNamespace = data('namespace', lastElementClicked);

        if (currentNamespace === 'payment') header.isTop();
        modal.close();
        header.closeMenu();
    });

    // set class to current menu voice
    Barba.Dispatcher.on('newPageReady', (view) => {
        const currentMenuVoice = getMenuVoice(menuVoices, view.namespace);
        removeClassByPrefix(document.body, 'page-');
        removeClass(document.body, 'is-modal');
        classie.add(document.body, `page-${view.namespace}`);
        if (currentMenuVoice !== undefined && currentMenuVoice !== null) {
            classie.add(currentMenuVoice, 'is-active');
        }
    });

    Barba.Dispatcher.on('transitionCompleted', (currentStatus, oldStatus, container) => {
        if (typeof oldStatus !== 'undefined') {
            animateIn();
        }
    });

    // get right transition
    Barba.Pjax.getTransition = () => {
        const transition = GenericTransition;
        const prevNamespace = Barba.Pjax.History.prevStatus().namespace;
        const prevMenu = getMenuVoice(menuVoices, prevNamespace);

        if (prevMenu !== undefined && prevMenu !== null) {
            classie.remove(prevMenu, 'is-active');
        }

        return transition;
    };

});

