import classie from 'classie';

const camelCaseRegExp = /-([a-z])/ig;
const toCamelCase = str => str.replace(camelCaseRegExp, match => match[1].toUpperCase());

export const byId = id => document.getElementById(id);

export const byClassName = (selector, ctx = document) => {
    Array.toArray(ctx.getElementsByClassName(selector));
};

export const qs = (selector, ctx = document) => ctx.querySelector(selector);

export const qsa = (selector, ctx = document) => Array.from(ctx.querySelectorAll(selector));

export const data = (attr, ctx) => ctx.hasAttribute('data-' + attr) ? ctx.getAttribute('data-' + attr) : '';

export const toggleClass = (el, className, toggle) => {
    const fn = toggle === undefined ? 'toggle' : (toggle ? 'add' : 'remove'); //eslint-disable-line no-nested-ternary
    classie[fn](el, className);
};

export const addClass = (el, className) => {
    classie.add(el, className);
};

export const removeClass = (el, className) => {
    classie.remove(el, className);
};

export const removeClassByPrefix = (el, prefix) => {
    const regx = new RegExp(`\\b${prefix}.*?\\b`, 'g');
    [...el.classList].map(className => {
        regx.test(className) && el.classList.remove(className);
    });
}

export const hasClass = (el, className) => classie['has'](el, className);

export const toNodeList = (elements) => new NodeList(elements);

export function handleEvent(eventName, {
    onElement,
    withCallback,
    useCapture = false,
} = {}, thisArg) {
    const element = onElement || document.documentElement;

    function handler(event) {
        if (typeof withCallback === 'function') {
            withCallback.call(thisArg, event);
        }
    }

    handler.destroy = function destroy() {
        return element.removeEventListener(eventName, handler, useCapture);
    };

    element.addEventListener(eventName, handler, useCapture);
    return handler;
}

export function createconfig(o = {}, config) {
    return Object.assign({}, config, o);
}

export const stringToDOM = (string = '') => {
    const fragment = document.createDocumentFragment();
    const wrapper = fragment.appendChild(document.createElement('div'));

    wrapper.innerHTML = string.trim();
    return wrapper.children[0];
};

export function index(element) {
    const sib = element.parentNode.childNodes;
    let n = 0;

    for (let i = 0; i < sib.length; i += 1) {
        if (sib[i] === element) return n;
        if (sib[i].nodeType === 1) n += 1;
    }

    return -1;
}

export function eq(parent, i) {
    return (i >= 0 && i < parent.length) ? parent[i] : -1;
}

export function inViewport(element) {
    const rect = element.getBoundingClientRect();
    const html = document.documentElement;

    return (rect.top >= 0 && rect.top < html.clientHeight) ||
        (rect.bottom >= 0 && rect.bottom < html.clientHeight);
}

export function getDevice() {
    let device = window.getComputedStyle(document.body, '::after').getPropertyValue('content');
    device = device.replace(/('|")/g, '');

    return device;
}

export function isMobile() {
    return !(getDevice() !== 'base' && getDevice() !== 'mobile' && getDevice() !== 's-tablet');
}

export function calculateRatio(width, height) {
    return width / height;
}

export function lerp(a, b, n) {
    return ((1 - n) * a) + (n * b);
}

export function viewportSize() {
    let coords = {
        x: window.innerWidth,
        y: window.innerHeight,
        ratio: window.innerWidth / window.innerHeight,
    };

    window.addEventListener('resize', () => {
        coords = {
            x: window.innerWidth,
            y: window.innerHeight,
            ratio: window.innerWidth / window.innerHeight,
        };
    });

    return coords;
}


export class NodeList {

    constructor(elements, ctx = document) {
        this.els = typeof elements === 'string' ? qsa(elements, ctx) : Array.from(elements);
    }

    toArray() {
        return this.els;
    }

    eq(index) {
        return this.els[index];
    }

    indexOf(target) {
        return this.els.indexOf(target);
    }

    attr(attr, value) {
        const { els } = this;
        const attrStr = toCamelCase(attr);
        if (value) {
            this.els.forEach((el) => (el[attrStr] = value));
            return this;
        }
        const el = els.length > 0 ? els[0] : undefined;
        const hook = NodeList.attrHooks[attrStr];
        if (!el) {
            return undefined;
        }
        return hook ? hook(el) : el[attrStr];
    }

    addClass(className) {
        this.els.forEach((el) => (classie.add(el, className)));
        return this;
    }

    removeClass(className) {
        this.els.forEach((el) => (classie.remove(el, className)));
        return this;
    }

    toggleClass(className, toggle) {
        const fn = toggle === undefined ? 'toggle' : (toggle ? 'add' : 'remove'); //eslint-disable-line no-nested-ternary
        this.els.forEach((el) => (classie[fn](el, className)));
        return this;
    }
}

NodeList.attrHooks = {
    'for': (el) => el.htmlFor,
    'class': (el) => el.className,
};

export function degrees(radians) {
    return (radians * 180) / Math.PI;
};

export function radians(degrees) {
    return (degrees * Math.PI) / 180;
};


export function customSplit(arr, type) {
    let text;
    const chars = [];
    let words;
    let lines;

    if (type === 'words') {
        arr.forEach((word, count) => {
            const children = Array.from(word.children);
            if (children.length > 0) {
                children.forEach((char) => {
                    chars.push(char.textContent);
                    char.parentNode.removeChild(char);
                });
                if (count < arr.length - 1) {
                    chars.push(' ');
                }
                words = chars.join('');
            }
        });
        text = words;
    } else if (type === 'lines') {
        arr.forEach((line, count) => {
            console.log(line);
            // lines.push(line.textContent);
        });
        lines.join('<br>');
        text = lines;
    } else if (type === 'chars') {
        arr.forEach((char) => {
            chars.push(char.textContent);
            char.parentNode.removeChild(char);
        });
        text = chars.join('');
    }

    return text;
}

