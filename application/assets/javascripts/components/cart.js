

import { addClass, removeClass, toggleClass, qs, qsa, inViewport } from '../base/utils';

class Cart {
  constructor(el) {
      this.el = el;
  }

  init() {
      this.addEvents();
  }

  open() {
      addClass(document.body, 'is-cart');
      qs('[data-modal="cart"]').setAttribute('data-status', 'active');
  }

  addEvents() {
      qs('.modal--cart .modal__bg', this.el).addEventListener('click', () => {
          this.close();
      });

      qs('.modal--cart .modal__close', this.el).addEventListener('click', () => {
          this.close();
      });
  }

  close() {
      removeClass(document.body, 'is-cart');
      qs('[data-modal="cart"]').setAttribute('data-status', '');
  }
}

export default Cart;