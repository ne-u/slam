import { qs, qsa, isMobile, addClass, removeClass, handleEvent } from '../base/utils';
import animations from '../animations/animations';

class Header {
    constructor(el) {
        this.el = el;
        this.scrollPos = 0;
        this.dropdownStatus = false;
        this.type = '';
    }

    init() {
        animations.get('header')(this.el, 'set');
        if (document.documentElement.scrollTop === 0) {
            this.el.removeAttribute('class');
            addClass(this.el, 'is-top');
        }
        this.addEvents();
    }

    animateIn() {
        animations.get('header')(this.el, 'init');
    }

    addEvents() {
        document.addEventListener('scroll', () => {
            this.scroll();
            this.scrollPos = document.documentElement.scrollTop;
        });

        qsa('[data-hover]', this.el).forEach((menu) => {
            menu.addEventListener('mouseenter', () => {
                if (!this.dropdownStatus) {
                    this.dropdownStatus = true;
                    if (this.type !== '' && this.type !== menu.getAttribute('data-hover')) {
                        animations.get('header')(this.el, 'hover-leave', this.type);
                    }
                    if (this.type !== menu.getAttribute('data-hover')) {
                        this.type = menu.getAttribute('data-hover');
                        this.dropDowns(this.type);
                    }
                }
            });
            menu.addEventListener('mouseleave', () => {
                if (this.dropdownStatus) {
                    this.dropdownStatus = false;
                    if (event.toElement === this.el) {
                        this.type = '';
                        animations.get('header')(this.el, 'hover-leave');
                        this.scroll();
                    }
                }
            });
        });

        qs('.dropdowns').addEventListener('mouseleave', () => {
            if (!qsa('[data-hover]').includes(event.toElement)) {
                this.closeMenu();
            }
        });
    }

    dropDowns(type) {
        animations.get('header')(this.el, 'hover', type);
    }

    isTop() {
        this.el.removeAttribute('class');
        addClass(this.el, 'is-top');
        removeClass(document.body, 'header-hide');
        animations.get('header')(this.el, 'top');
    }

    isFixed() {
        this.el.removeAttribute('class');
        addClass(this.el, 'is-fixed');
        if (this.scrollPos >= document.documentElement.scrollTop || this.dropdownStatus) {
            animations.get('header')(this.el, 'fixed');
            removeClass(this.el, 'is-hide');
            removeClass(document.body, 'header-hide');
        } else {
            addClass(this.el, 'is-hide');
            addClass(document.body, 'header-hide');
            animations.get('header')(this.el, 'hide');
        }
    }

    scroll() {
        if (document.documentElement.scrollTop > this.el.offsetHeight) {
            this.isFixed();
        } else {
            this.isTop();
        }
    }

    closeMenu() {
        this.dropdownStatus = false;
        this.type = '';
        animations.get('header')(this.el, 'hover-leave');
        this.scroll();
    }

}

export default Header;