import gsap from 'gsap';
import preload from 'preload-js';

import Component from '../base/component';
import { addClass, toggleClass } from '../base/utils';

//const SplitText = require('../../lib/gsap/uncompressed/utils/SplitText');

class Preloader extends Component {
    constructor(el, opts) {
        super(el, opts);

        this.completed = null;
        this.queue = new preload.LoadQueue(false);
        //this.splitTitle = new SplitText('.loader__title', { type: 'chars' });
    }

    init() {
        super.init();
        addClass(document.body, 'is-loading');
        this.queue.on('complete', this.allComplete, this);
        this.queue.loadManifest(this.options.assets, false);

        const tl = new gsap.TimelineMax({ onComplete: () => {
            this.queue.load();
        } });

        //here you can change the value of a to anything you need

        tl.add('start');

        tl.set('.loader__progress', { scaleY: 0, transformOrigin: '0 100%' })
        .set('.transition', { scaleY: 0, transformOrigin: '0 100%' })
        .set('.loader__logo img', { scale: 1, transformOrigin: '50% 50%' })
        .set('.loader__title', { autoAlpha: 1 })
        //.from('.loader__logo', 0.3, { yPercent: 20, autoAlpha: 0, ease: gsap.Expo.easeOut })
        .from('.loader__logo img', 1, { scale: 1.3, autoAlpha: 0, ease: gsap.Expo.easeOut }, 'start')
        .staggerFrom('.loader__title svg > *', 0.8, {
            y: '-101%',
            ease: gsap.Expo.easeOut
        }, 0.03, 'start+=0.5')
        .to('.loader__progress', 1.2, { scaleY: 1, ease: gsap.Expo.easeOut });

        // this.queue.on('progress', (e) => {
        //     // for (let i = 1; i < 2; i += 1) {
        //     // }
        //     //gsap.to('.loader__progress', 0.3, { scaleY: e.progress });
        // });

        return new Promise((resolve) => { this.completed = resolve; });
    }
    
    allComplete() {
        const tl = new gsap.TimelineMax();
        tl.add('start')
        .to('.loader__logo img', 1, { scale: 0.8, autoAlpha: 0, ease: gsap.Expo.easeOut }, 'start')
        .staggerTo('.loader__title svg > *', 0.8, {
            y: '-100%',
            ease: gsap.Expo.easeOut
        }, 0.03, 'start+=0.25')
        .to('.loader', 0.8, { yPercent: -100, transformOrigin: '0 0%', ease: gsap.Expo.easeOut }, 'start+=0.5');

        setTimeout(() => {
            this.completed();
        }, tl.duration() / 1.2);
    }

    onComplete() {
        const percent = parseInt(this.queue.progress * 100);
        this.loaded.innerHTML = percent;
    }

    destroy() {
        super.destroy();

        addClass(document.body, 'is-loaded');
        toggleClass(document.body, 'is-loading');
    }
}

export default Preloader;