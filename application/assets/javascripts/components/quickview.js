import { addClass, removeClass, toggleClass, qs, qsa, inViewport } from '../base/utils';

class Quickview {
    constructor(el) {
        this.el = el;

        this.details = qsa('.product__detail li', el);
    }

    init() {
        this.addEvents();
    }

    destroy() {
        removeClass(document.body, 'is-modal');
        qs('[data-modal="quick-view"]').setAttribute('data-status', '');
    }

    addEvents() {

        const quickBtn = qsa('.product__item__badge--quickview', document);

        quickBtn.forEach((btn) => {
            btn.addEventListener('click', () => {
                this.open();
            });
        });

        qs('.modal__bg', this.el).addEventListener('click', () => {
            this.close();
        });


        qs('.modal__close', this.el).addEventListener('click', () => {
            this.close();
        });

        this.details.forEach(el => {
            el.addEventListener('click', () => {
                const type = el.parentNode.className;
                if (!el.hasAttribute('data-oos')) this.toggleDetail(el, type);
            });
        });

        qs('.btn--primary', this.el).addEventListener('click', () => {
            addClass(document.body, 'is-cart');
            qs('[data-modal="cart"]').setAttribute('data-status', 'active');
            addClass(qs('.cart__badge', document), 'cart__badge--active');
            qs('.cart__badge', document).textContent = '1';
        });
    }

    open() {
        this.el.setAttribute('data-status', 'active');
        toggleClass(document.body, 'is-modal');
    }

    close() {
        this.el.setAttribute('data-status', '');
        toggleClass(document.body, 'is-modal');
    }

    toggleDetail(el, type) {
        if (type === 'color') {
            if (qs('.color--active')) removeClass(qs('.color--active'), 'color--active');
            addClass(el, 'color--active');
        } else if (type === 'size') {
            if (qs('.size--active')) removeClass(qs('.size--active'), 'size--active');
            addClass(el, 'size--active');
            removeClass(qs('.btn--disabled'), 'btn--disabled');
        }
    }
}

export default Quickview;