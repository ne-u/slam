import { qs, qsa, isMobile, addClass, removeClass, handleEvent } from '../base/utils';
import animations from '../animations/animations';


class Slider {
    constructor(ctx) {
        this.ctx = ctx || document;
        this.slides = qsa('[data-slide]', this.ctx);
        this.totSlides = this.slides.length - 1;
        this.type = ctx.getAttribute('data-slider');

        this.index = 0;
        this.indexPrev = this.totSlides;
        this.indexNext = this.index + 1;

        this.btnPrev = qs('.slider__navigator__arrow--left', this.ctx);
        this.btnNext = qs('.slider__navigator__arrow--right', this.ctx);

        this.counterCurrent = qs('.slider__navigator--lines', this.ctx).children[this.index];
        this.counterLast = '';

        this.slideCurrent = null;
        this.slidePrev = null;
        this.slideNext = null;
    }

    get getIndex() {
        return this.index;
    }

    init() {
        if (this.ctx === qs('[data-slider="hero"]')) {
            animations.get('hero')('enter', this.slides[this.index], '', true, false, this.index);
            animations.get('hero-info')(qs('.hero__info__dots', this.slides[this.index]));
            qs('.hero__info__dots', this.slides[this.index]).addEventListener('mouseenter', () => animations.get('hero-info')(qs('.hero__info__dots', this.slides[this.index]), 'hover'));
            qs('.hero__info__dots', this.slides[this.index]).addEventListener('mouseleave', () => animations.get('hero-info')(qs('.hero__info__dots', this.slides[this.index]), 'leave'));
        } else if (this.ctx === qs('[data-slider="gallery"]')) {
            animations.get('gallery')(this.slides[this.index], '', true);
        }
        animations.get('counter')([], this.counterCurrent, true);
        this.updateSlide();
        this.addEvents();
    }

    addEvents() {
        this.btnPrev.addEventListener('click', () => this.update('prev'));
        this.btnNext.addEventListener('click', () => this.update('next'));
    }

    update(dir) {

        if (dir === 'prev') {
            this.index -= 1;
            this.index = this.index < 0 ? this.totSlides : this.index;
        } else if (dir === 'next') {
            this.index += 1;
            this.index = this.index > this.totSlides ? 0 : this.index;
        }

        this.indexPrev = this.index === 0 ? this.totSlides : this.index - 1;
        this.indexNext = this.index === this.totSlides ? 0 : this.index + 1;

        const ex = dir === 'prev' ? this.indexNext : this.indexPrev;

        this.resetState();
        this.updateSlide();

        this.counterLast = qs('.slider__navigator--lines', this.ctx).children[ex];
        this.counterCurrent = qs('.slider__navigator--lines', this.ctx).children[this.index];

        animations.get('counter')(this.counterLast, this.counterCurrent, false);
        if (this.ctx === qs('[data-slider="hero"]')) animations.get('hero')('enter', this.slides[this.index], dir, false, this.slides[ex], this.index, ex);
        if (this.ctx === qs('[data-slider="gallery"]')) animations.get('gallery')(this.slides[this.index], this.slides[ex], false, dir);

    }

    updateSlide() {
        this.slidePrev = this.slides[this.indexPrev];
        this.slideCurrent = this.slides[this.index];
        this.slideNext = this.slides[this.indexNext];
        this.slidePrev.setAttribute('data-slide', 'prev');
        this.slideCurrent.setAttribute('data-slide', 'active');
        this.slideNext.setAttribute('data-slide', 'next');

    }

    resetState() {
        this.slidePrev.setAttribute('data-slide', '');
        this.slideCurrent.setAttribute('data-slide', '');
        this.slideNext.setAttribute('data-slide', '');
    }

    animate() {

    }

    counter() {

        animations.get('counter')();
    }
}

export default Slider;