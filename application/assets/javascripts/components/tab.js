import { qs, qsa, isMobile, addClass, removeClass, hasClass, handleEvent, inViewport } from '../base/utils';
import animations from '../animations/animations';


class Tab {
    constructor(ctx) {
        this.ctx = ctx || document;
        this.slides = qsa('[data-tab]', this.ctx);
        this.type = ctx.getAttribute('data-tabs');
        this.fx = ctx.getAttribute('data-fx') || 'tab-fade';
        this.tabActive = ctx.firstElementChild.getAttribute(`data-${this.type}`);
        this.btnClick = qsa(`.tab__btn[data-${this.type}]`, document);
        this.btnHover = qsa(`.tab__btn__hover[data-${this.type}]`, document);
    }

    get getIndex() {
        return this.index;
    }

    init() {

        this.update(this.tabActive);
        this.addEvents();
        document.addEventListener('scroll', () => {
            if (inViewport(this.ctx) && !this.ctx.hasAttribute('data-animated')) {
                animations.get(this.fx)([], this.ctx.firstElementChild);
                this.ctx.setAttribute('data-animated', true);
            }
        });
    }

    addEvents() {
        this.btnClick.forEach((btn) => {
            btn.addEventListener('click', () => {
                this.btnClick.forEach((el) => {
                    removeClass(el, 'tab__btn--active');
                });
                addClass(btn, 'tab__btn--active');
                const type = this.type;
                const active = btn.getAttribute(`data-${type}`);
                this.update(active);
            });
        });

        this.btnHover.forEach((btn) => {
            btn.addEventListener('mouseover', () => {
                if (!hasClass(btn, 'tab__btn--active')) {
                    this.btnHover.forEach((el) => {
                        removeClass(el, 'tab__btn--active');
                    });
                    addClass(btn, 'tab__btn--active');
                    const type = this.type;
                    const active = btn.getAttribute(`data-${type}`);
                    this.update(active);
                }
            });
        });
    }

    update(active) {
        let ex;
        let current;
        this.tabActive = active;


        this.slides.forEach((el) => {
            if (el.getAttribute(`data-${this.type}`) === active) {
                el.setAttribute('data-tab', 'active');
                current = el;
            } else {
                el.setAttribute('data-tab', '');
                ex = el;
            }
        });
        animations.get(this.fx)(ex, current);
    }
}

export default Tab;