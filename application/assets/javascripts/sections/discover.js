import Barba from 'barba.js';
import { addClass, toggleClass, qs, qsa, inViewport } from '../base/utils';
import Slider from '../components/slider';
import animations from '../animations/animations';


const Discover = Barba.BaseView.extend({
    namespace: 'discover',
    onEnter() {
        const container = Discover.container;

        const sliderGallery = new Slider(qs('[data-slider="gallery"]', document));
        

        sliderGallery.init();

    },

    onLeave() {
    }
});

Discover.init();