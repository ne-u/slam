import gsap from 'gsap';

import { qs, qsa, isMobile } from '../base/utils';


export default {
    onEnter() {
        // const scrollCont = qs('[data-scrollbar]', this.container);
        // this.scrollbar = Scrollbar.init(scrollCont);

        // const pageContainer = qs('.c-content-block', this.container);
        // const pageContent = qs('.c-page-cont', this.container);

        // if (!isMobile()) {
        //     LogoManager.hide();
        // }

        // this.resizeContainer = () => {
        //     const containerH = window.innerHeight - 160;
        //     const contentH = pageContent.clientHeight;

        //     if (contentH > containerH) {
        //         pageContainer.style.height = 'auto';
        //     } else if (contentH <= containerH) {
        //         pageContainer.style.height = '100vh';
        //     }
        // };
        // this.resizeContainer();

        // this.resizeScrollbar = () => {
        //     this.scrollbar.update();

        //     this.resizeContainer();
        // };
        // window.addEventListener('resize', this.resizeScrollbar);
    },

    onEnterCompleted() {
        const elsToChange = qsa('[data-animation]', this.container);

        elsToChange.forEach((el) => {
            el.setAttribute('data-animation', 'out');
        });
    },

    onLeave() {
        window.removeEventListener('resize', this.resizeScrollbar);
        this.scrollbar.destroy();
    }
};