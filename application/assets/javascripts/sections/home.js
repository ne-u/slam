import Barba from 'barba.js';
import { addClass, toggleClass, qs, qsa } from '../base/utils';
import Slider from '../components/slider';
import Tab from '../components/tab';
import animations from '../animations/animations';
let sliderHero;

const Homepage = Barba.BaseView.extend({
    namespace: 'home',

    onEnter() {
        const container = Homepage.container;
        animations.get('home')(container, false);

        sliderHero = new Slider(qs('[data-slider="hero"]', document));
        sliderHero.init();

        const tabs = Array.from(qsa('[data-tabs]', document));
        tabs.forEach((el) => {
            const tab = new Tab(el);
            tab.init();
        });
        
    },

    onLeave() {
    }
});

export default Homepage;