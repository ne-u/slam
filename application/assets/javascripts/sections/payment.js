import gsap from 'gsap';
import Barba from 'barba.js';
import { addClass, removeClass, toggleClass, qs, qsa, inViewport } from '../base/utils';
import animations from '../animations/animations';


const Payment = Barba.BaseView.extend({
    namespace: 'payment',
    onEnter() {

        const container = Payment.container;
        const steps = qsa('.step__item', container);

        gsap.set(qsa('.completed', container), { className: '+=heightZero' });

        steps.forEach(step => {
            if (qs('a[href="#next"]', step)) {
                qs('a[href="#next"]', step).addEventListener('click', () => {
                    event.preventDefault();
                    //if (qs('.step__item--active')) removeClass(qs('.step__item--active'), 'step__item--active');
                    if(step !== qs('.step__item--notification'))  {
                        animations.get('step')(step, true);
                    } else {
                        addClass(qs('.btn.btn--first'), 'btn--hide');
                        addClass(qs('.btn.btn--second'), 'btn--hide');
                    }
                    addClass(step, 'step__item--completed');
                    removeClass(step.nextElementSibling, 'step__item--not-active');
                });
            }

            if (qs('a[href="#edit"]', step)) {
                qs('a[href="#edit"]', step).addEventListener('click', () => {
                    event.preventDefault();
                    removeClass(step, 'step__item--completed');
                    animations.get('step')(step, false);
                    addClass(step.nextElementSibling, 'step__item--not-active');
                });
            }

            if (qs('a[href="#login"]', step)) {
                qs('a[href="#login"]', step).addEventListener('click', () => {
                    event.preventDefault();
                    //if (qs('.step__item--active')) removeClass(qs('.step__item--active'), 'step__item--active');
                    animations.get('step')(step, true);
                    addClass(step, 'step__item--completed');
                });
            }

            if (qs('.modal__close', step)) {
                qs('.modal__close', step).addEventListener('click', () => {
                    removeClass(step, 'step__item--completed');
                    animations.get('step')(step, false);
                });
            }
        });

        qs('.back').addEventListener('click', () => {
            event.preventDefault();
            addClass(qs('.step__wrapper--second'), 'step__wrapper--hide');
            removeClass(qs('.step__wrapper--first'), 'step__wrapper--hide');
            animations.get('step-wrapper')(qs('.step__wrapper--first'));
        });

        qs('a[href="#payment-confirmation"]').addEventListener('click', () => {
            event.preventDefault();
            addClass(qs('.step__wrapper--first'), 'step__wrapper--hide');
            removeClass(qs('.step__wrapper--second'), 'step__wrapper--hide');
            animations.get('step-wrapper')(qs('.step__wrapper--second'));
        });

        qs('a[href="#thank-you"]').addEventListener('click', () => {
            event.preventDefault();
            addClass(qs('main'), 'thank-you');
            addClass(qs('.step__wrapper--second'), 'step__wrapper--hide');
            removeClass(qs('.step__wrapper--third'), 'step__wrapper--hide');
            animations.get('step-wrapper')(qs('.step__wrapper--third'));
        });

    },

    onLeave() {
    }
});

Payment.init();