import gsap from 'gsap';
import Barba from 'barba.js';
import { addClass, removeClass, toggleClass, qs, qsa, inViewport } from '../base/utils';
import Quickview from '../components/quickview';
import animations from '../animations/animations';

const Products = Barba.BaseView.extend({
    namespace: 'products',
    onEnter() {
        const container = Products.container;
        const panelFilter = qs('.panel--filter', document);
        const modal = new Quickview(qs('.modal[data-modal="quick-view"]'), document);

        gsap.set(qsa('.panel--filter .l-col', panelFilter), { autoAlpha: 0 });


        modal.init();

        let active = false;
        qs('.filter-toggle', document).addEventListener('click', () => {
            let dir;
            if (!active) {
                dir = 'enter';
                active = true;
            } else {
                dir = 'leave';
                active = false;
            }
            toggleClass(event.target, 'filter-toggle--active');
            animations.get('filter')(panelFilter, dir);
        });

        const selects = qsa('.select-promo__item', document);
        selects.forEach(select => {
            select.addEventListener('click', () => {
                removeClass(qs('.select-promo__item--active'), 'select-promo__item--active');
                addClass(select, 'select-promo__item--active');
            });
        });
    },

    onLeave() {
    }
});

Products.init();