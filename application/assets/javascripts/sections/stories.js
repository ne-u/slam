import Barba from 'barba.js';
import { addClass, toggleClass, qs, qsa, inViewport } from '../base/utils';
import animations from '../animations/animations';


const Stories = Barba.BaseView.extend({
    namespace: 'stories',
    onEnter() {

        const container = Stories.container;
        animations.get('stories')(container);
    },

    onLeave() {
    }
});

Stories.init();