import Barba from 'barba.js';
import { addClass, toggleClass, qs, qsa, inViewport } from '../base/utils';
import Slider from '../components/slider';
import animations from '../animations/animations';


const Story = Barba.BaseView.extend({
    namespace: 'story',
    onEnter() {
        const container = Story.container;

        const sliderGallery = new Slider(qs('[data-slider="gallery"]', document));
        const share = qs('.share', container);
        const title = qs('.hero__title h1', container);

        sliderGallery.init();

        if (inViewport(title) && !title.getAttribute('data-storytitle') === true) {
            animations.get('story-title')(title);
            title.setAttribute('data-storytitle', true);
        }
        document.addEventListener('scroll', () => {
            if (inViewport(title) && !title.getAttribute('data-storytitle') === true) {
                animations.get('story-title')(title);
                title.setAttribute('data-storytitle', true);
            }
        });

        if (inViewport(share) && !share.getAttribute('data-share') === true) {
            animations.get('inview-share')(share);
            share.setAttribute('data-share', true);
        }
        document.addEventListener('scroll', () => {
            if (inViewport(share) && !share.getAttribute('data-share') === true) {
                animations.get('inview-share')(share);
                share.setAttribute('data-share', true);
            }
        });

    },

    onLeave() {
    }
});

Story.init();