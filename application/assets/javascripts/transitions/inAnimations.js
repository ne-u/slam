import gsap from 'gsap';
import { qsa, viewportSize, removeClass, hasClass } from '../base/utils';

const transitions = {
    generic() {
        const tl = new gsap.TimelineMax({ onComplete: () => {
            gsap.set('.transition', { scaleY: 0, transformOrigin: '0, 0' });
            gsap.set('.transition__bg', { scaleY: 0, transformOrigin: '0, 0' });
            gsap.set(this.newContainer, { clearProps: 'all' });
            gsap.to('#header', 0.8, {
                backgroundColor: 'transparent',
                ease: gsap.Expo.easeOut
            });
            removeClass(document.body, 'is-trans');

            this.done();
        } });

        tl.add('start')
        .set(this.newContainer, { autoAlpha: 0, y: viewportSize().y / 3 })
        .to('.transition', 0.8, {
            scaleY: 0,
            transformOrigin: '0 0',
            ease: gsap.Expo.easeOut
        }, 'start')
        .to(this.newContainer, 0.6, {
            autoAlpha: 1,
            y: 0,
            ease: gsap.Expo.easeOut
        }, 'start+=0.2');

    }

    // stagger() {
    //     const tl = new gsap.TimelineMax({ onComplete: () => { this.done(); } });
    //     this.setNewElement();

    //     tl.add('start')
    //     .fromTo(this.newContainer, 0.1, {
    //         autoAlpha: 0
    //     }, {
    //         autoAlpha: 1
    //     }, 'start')
    //     .staggerTo(this.elsIn, 2, {
    //         autoAlpha: 1
    //     }, 0.2);
    // },

    // fade() {
    //     gsap.fromTo(this.newContainer, 0.1, {
    //         autoAlpha: 0
    //     }, {
    //         autoAlpha: 1,
    //         onComplete: () => { this.done(); }
    //     }, 'start');
    // }
};

export default transitions;