import Barba from 'barba.js';
import HomeToAll from './homeToAll';

import ins from './inAnimations';
import outs from './outAnimations';

const GenericTransition = Barba.BaseTransition.extend({
    name: 'generic',
    start() {
        Promise.all([
            this.newContainerLoading,
            outs.generic.bind(this)()
        ]).then(ins.generic.bind(this));
    }
});

export { GenericTransition };