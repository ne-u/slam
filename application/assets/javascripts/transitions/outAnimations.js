import gsap from 'gsap';
import { qsa, addClass } from '../base/utils';

const transitions = {
    generic() {
        addClass(document.body, 'is-trans');
        return new Promise((resolve) => {
            const tl = new gsap.TimelineMax({ onComplete: () => {
                resolve();
                window.scrollTo(0, 0);
            } });
            tl.add('start')
            .set('.transition', { scaleY: 0, transformOrigin: '0 100%' })
            .set('.transition__bg', { scaleY: 0, transformOrigin: '0 100%' })
            .to('.transition', 0.8, {
                scaleY: 1,
                ease: gsap.Expo.easeOut
            }, 'start')
            .to('.transition__bg', 0.8, {
                scaleY: 1,
                ease: gsap.Expo.easeOut
            }, 'start+=0.5')
            .set(this.oldContainer, {
                autoAlpha: 0,
                display: 'none'
            });

        });
    }
};

export default transitions;